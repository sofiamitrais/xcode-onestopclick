//
//  LoadingView.swift
//  onestopclick
//
//  Created by Sofia on 06/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import UIKit

class LoadingView:UIActivityIndicatorView
{
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        self.hidesWhenStopped = true
        self.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.startAnimating()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
}
