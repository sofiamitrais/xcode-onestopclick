//
//  FirstViewController.swift
//  onestopclick
//
//  Created by Sofia on 04/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {

    var homeData = [ListProduct]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        getData()
    }
    
    func downloadImage(section:Int, row:Int, url: URL)
    {
        ApiService.dataTask(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() 
            {
                let cell = self.tableView.cellForRow(at: IndexPath(row: row, section: section))
                if let imageView = cell?.viewWithTag(2) as? UIImageView
                {
                    imageView.image = UIImage(data: data)
                }
            }
        }
    }
    
    func finishGet (message:String, data:Data?) -> Void
    {
        do
        {
            if let jsonData = data
            {
                let parsedData = try JSONDecoder().decode([ListProduct].self, from: jsonData)
                self.homeData = parsedData
                
                DispatchQueue.main.async { [unowned self] in
                    self.tableView.reloadData()
                }
            }
        }
        catch
        {
            print("Parse Error: \(error)")
        }
    }
    
    func getData()
    {
        let homeUrl = ApiService.getUrl(key: "home")
        if let url = homeUrl
        {
            ApiService.callGet(url: url, finish: finishGet)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.homeData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeData[section].products.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.homeData[section].name
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        let product = self.homeData[indexPath.section].products[indexPath.row]

        if let nameLabel = cell.viewWithTag(3) as? UILabel
        {
            nameLabel.text = product.product_name
            nameLabel.sizeToFit()
        }
        
        if let priceLabel = cell.viewWithTag(4) as? UILabel
        {
            priceLabel.text = product.getPrice()
        }
        
        let imageUrlStr = product.images[0].image_url
        if let imageUrl = URL(string: imageUrlStr)
        {
            downloadImage(section : indexPath.section, row: indexPath.row, url : imageUrl)
        }
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "product")
        {
            if let cell = sender as? UITableViewCell
            {
                if let indexPath = self.tableView.indexPath(for: cell)
                {
                    let product = self.homeData[indexPath.section].products[indexPath.row]
                    
                    let productController = segue.destination as? ProductViewController
                    productController?.updateData(product: product)
                }
            }
        }
        
    }
}

