//
//  CategoryViewController.swift
//  onestopclick
//
//  Created by Sofia on 05/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import UIKit

class CategoryViewController: UICollectionViewController {
    
    var categoryData = [Category]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        getData()
    }
    
    func finishGet (message:String, data:Data?) -> Void
    {
        do
        {
            if let jsonData = data
            {
                let parsedData = try JSONDecoder().decode([Category].self, from: jsonData)
                self.categoryData = parsedData
                
                DispatchQueue.main.async { [unowned self] in
                    self.collectionView?.reloadData()
                }
            }
        }
        catch
        {
            print("Parse Error: \(error)")
        }
    }
    
    func getData()
    {
        let categoryUrl = ApiService.getUrl(key: "category")
        if let url = categoryUrl
        {
            let keychain = KeychainSwift()
            let token = keychain.get("bearer_token")
            print("Token di category \(token)")
            
            ApiService.callGet(url: url, finish: finishGet)
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryData.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UICollectionViewCell
        
        if let label = cell.viewWithTag(2) as? UILabel
        {
            label.text = self.categoryData[indexPath.row].name
        }
        
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "product")
        {
            if let cell = sender as? UICollectionViewCell
            {
                if let indexPath = self.collectionView?.indexPath(for: cell)
                {
                    let category = self.categoryData[indexPath.row]
                    
                    let detailController = segue.destination as? CategoryDetailViewController
                    detailController?.updateData(category: category)
                }
            }
        }
        
    }
}

