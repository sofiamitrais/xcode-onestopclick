//
//  ProductViewController.swift
//  onestopclick
//
//  Created by Sofia on 04/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//
import UIKit

class ProductViewController: UIViewController {
 
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadImage(url: URL)
    {
        ApiService.dataTask(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async{ [unowned self] in
                if let imageView = self.view.viewWithTag(1) as? UIImageView
                {
                    imageView.image = UIImage(data: data)
                }
            }
        }
    }
  
    func updateData(product:Product)
    {
        self.title = product.product_name
        
        if let nameLabel = self.view.viewWithTag(2) as? UILabel
        {
            nameLabel.text = product.product_name
        }
        
        if let priceLabel = self.view.viewWithTag(3) as? UILabel
        {
            priceLabel.text = product.getPrice()
        }
        
        if let descriptionLabel = self.view.viewWithTag(4) as? UILabel
        {
            descriptionLabel.text = product.description
            descriptionLabel.sizeToFit()
        }
        
        let imageUrlStr = product.images[0].image_url
        if let imageUrl = URL(string: imageUrlStr)
        {
            self.downloadImage(url : imageUrl)
        }
    }
}


