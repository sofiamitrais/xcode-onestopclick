//
//  RegisterViewController.swift
//  onestopclick
//
//  Created by Sofia on 05/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    var isSuccessRegister = false
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfirmationTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        
        TextFieldHelper.setup(textField: nameTextField)
        TextFieldHelper.setup(textField: emailTextField)
        TextFieldHelper.setup(textField: passwordTextField)
        TextFieldHelper.setup(textField: passwordConfirmationTextField)
        
    }
    
    func finishAlert(alert: UIAlertAction!)
    {
        if(self.isSuccessRegister)
        {
            FormHelper.clear(fields: [ self.nameTextField, self.emailTextField, self.passwordTextField, self.passwordConfirmationTextField ])
            performSegue(withIdentifier: "register", sender: nil)
        }
    }
    
    func finishPost (message:String, data:Data?) -> Void
    {
        do
        {
            if let jsonData = data
            {
                let parsedData = try JSONDecoder().decode(RegisterResponse.self, from: jsonData)
                
                DispatchQueue.main.async { [unowned self] in
                    
                    var title = "Error"
                    
                    if let code = parsedData.code, code == 200
                    {
                        self.isSuccessRegister = true
                        title = "Success"
                    }

                    self.dismiss(animated: true, completion: {
                        AlertHelper.showMessage(view: self, title: title, message: parsedData.message, handler: self.finishAlert)
                    })
                }
            }
        }
        catch
        {
            print("Parse Error: \(error)")
        }
    }
    
    func register()-> Bool
    {
        var data = [String:UITextField]()
        data["name"] = nameTextField
        data["email"] = emailTextField
        data["password"] = passwordTextField
        data["password_confirmation"] = passwordConfirmationTextField
        
        let extraRules = [
            "email" : "isEmail"
        ]
        let params = FormHelper.validate(data: data, rules: extraRules)
        
        if(params.count == data.count)
        {
            let registerUrl = ApiService.getUrl(key: "register")
            if let url = registerUrl
            {
                AlertHelper.showLoading(view: self)
                ApiService.callPost(url: url, params: params, finish: finishPost)
            }
        }
        
        return false
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if(identifier == "register")
        {
            return register()
        }
        
        return true
    }
}
