//
//  LoginViewController.swift
//  onestopclick
//
//  Created by Sofia on 05/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews()
    {
        TextFieldHelper.setup(textField: emailTextField)
        TextFieldHelper.setup(textField: passwordTextField)
    }
    
    func finishPost (message:String, data:Data?) -> Void
    {
        do
        {
            if let jsonData = data
            {
                let parsedData = try JSONDecoder().decode(LoginResponse.self, from: jsonData)
                DispatchQueue.main.async { [unowned self] in
                    
                    self.dismiss(animated: true, completion: {
                        
                        if let token = parsedData.access_token
                        {
                            let keychain = KeychainSwift()
                            keychain.set(token, forKey: "bearer_token")
                            
                            FormHelper.clear(fields: [ self.emailTextField, self.passwordTextField ])
                            self.performSegue(withIdentifier: "login", sender: nil)
                        }
                        else
                        {
                            AlertHelper.showMessage(view: self, title: "Error", message: parsedData.message!, handler: nil)
                        }
                        
                    })
                }
            }
        }
        catch
        {
            print("Parse Error: \(error)")
        }
    }
    
    func login()-> Bool
    {
        var data = [String:UITextField]()
        data["username"] = emailTextField
        data["password"] = passwordTextField
        
        let extraRules = [
            "username" : "isEmail"
        ]
        let params = FormHelper.validate(data: data, rules: extraRules)
        
        if(params.count == data.count)
        {
            let loginUrl = ApiService.getUrl(key: "login")
            if let url = loginUrl
            {
                AlertHelper.showLoading(view: self)
                ApiService.callPost(url: url, params: params, finish: finishPost)
            }
        }
        
        return false
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if(identifier == "login")
        {
            return login()
        }
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


