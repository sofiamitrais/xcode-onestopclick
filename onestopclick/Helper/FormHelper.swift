//
//  FormHelper.swift
//  onestopclick
//
//  Created by Sofia on 05/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import Foundation
import UIKit

class FormHelper
{
    static func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    static func validateText(string:String, rule:String)->Bool
    {
        var isError = true
        switch rule
        {
            case "isEmail":
                isError = !self.isValidEmailAddress(emailAddressString: string)
            default:
                isError = true
        }
        
        return isError
    }
    
    static func validate(data:[String:UITextField], rules:[String:String]) -> [String:String]
    {
        var params = [String:String]()
        
        for (key, textField) in data
        {
            var isError = false
            if let str = textField.text as String!, !str.isEmpty
            {
                if let rule = rules[key]
                {
                    isError = FormHelper.validateText(string:str, rule:rule)
                }
                
                if(isError == false)
                {
                    params[key] = str
                    TextFieldHelper.setup(textField: textField)
                }
            }
            else
            {
                isError = true
            }
                
            if(isError)
            {
                TextFieldHelper.error(textField: textField)
            }
        }
        
        return params
    }
    
    static func clear(fields: [UITextField])
    {
        for field in fields
        {
            field.text = ""
        }
    }
}
