//
//  AlertHelper.swift
//  onestopclick
//
//  Created by Sofia on 06/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import UIKit

class AlertHelper
{
    static func showLoading(view:UIViewController)
    {
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = LoadingView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        alert.view.addSubview(loadingIndicator)
        
        view.present(alert, animated: true, completion: nil)
    }
    
    static func showMessage(view:UIViewController, title:String, message:String, handler: ((UIAlertAction) -> Void)? = nil)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: handler))
        view.present(alert, animated: true, completion: nil)
    }
}
