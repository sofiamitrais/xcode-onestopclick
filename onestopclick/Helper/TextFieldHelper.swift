//
//  TextFieldHelper.swift
//  onestopclick
//
//  Created by Sofia on 05/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import Foundation
import UIKit

class TextFieldHelper
{
    static func setup(textField:UITextField)
    {
        let borderColor = "#d3d3d3";
        TextFieldHelper.borderBottom(textField: textField, borderWidth: 1.0, color: ColorHelper.hexStringToUIColor(hex: borderColor))
    }
    
    static func error(textField:UITextField)
    {
        TextFieldHelper.borderBottom(textField: textField, borderWidth: 1.0, color: UIColor.red)
    }
    
    static func borderBottom(textField:UITextField, borderWidth: Float, color:UIColor)
    {
        let border = CALayer()
        let width = CGFloat(borderWidth)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }
    
}
