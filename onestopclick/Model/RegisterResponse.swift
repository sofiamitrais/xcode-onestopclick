//
//  RegisterResponse.swift
//  onestopclick
//
//  Created by Sofia on 06/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

struct RegisterResponse:Decodable
{
    let message:String
    let code:Int?
}
