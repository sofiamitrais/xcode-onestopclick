//
//  LoginResponse.swift
//  onestopclick
//
//  Created by Sofia on 06/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

struct LoginResponse:Decodable
{
    let error:String?
    let message:String?
    
    let token_type:String?
    let expires_in:UInt32?
    let access_token:String?
}
