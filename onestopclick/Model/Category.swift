//
//  Category.swift
//  onestopclick
//
//  Created by Sofia on 06/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

struct Category :Decodable
{
    let id:Int
    let name:String
    let target:String
    let priority:Int
    let is_active:Int
}
