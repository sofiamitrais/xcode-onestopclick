//
//  Product.swift
//  onestopclick
//
//  Created by Sofia on 04/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//
struct Product: Decodable
{
    let id:Int
    let product_name:String
    let price:String
    let description:String
    
    let images: [Image]
    
    func getPrice()->String
    {
        if let amount = Float(price)
        {
            return String(format: "Rp %.02f", amount)
        }
        return ""
    }
}
