//
//  ListProductModel.swift
//  onestopclick
//
//  Created by Sofia on 04/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

struct ListProduct:Decodable
{
    let id:Int
    let name:String
    let products: [Product]
}
