//
//  ApiSevice.swift
//  onestopclick
//
//  Created by Sofia on 04/10/17.
//  Copyright © 2017 Sofia. All rights reserved.
//

import Foundation

class ApiService
{
    static let apiUrl = "http://onestopclick.tk/"
//    static let apiUrl = "http://localhost:8000/"
    
    static let urls:[String:String] = [
        
        "home": "home/products",
        "category": "api/get_categories",
        
        "register": "api/auth/register",
        "login": "api/auth/token"
    ]
    
    static func getUrl(key:String) -> URL?
    {
        return URL(string: self.apiUrl + self.urls[key]!)
    }
    
    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    
    static func callPost(url:URL, params:[String:Any], finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let postString = self.getPostString(params: params)
        request.httpBody = postString.data(using: .utf8)
        
        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                result.message = "Success"
                result.data = data
            }
            
            finish(result)
        }
        task.resume()
    }
    
    static func callGet(url:URL, finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: url){(data, response, error) in
        
            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                result.message = "Success"
                result.data = data
            }
            
            finish(result)
            
        }
        task.resume()
    }
    
    static func dataTask(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ())
    {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
}
